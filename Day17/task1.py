#! /usr/bin/env python3
import itertools

cubes = {}
with open("input.txt") as f:
	data = f.read().splitlines()

for y, line in enumerate(data): # Populate initial data
	for x, char in enumerate(line):
		if char == "#":
			cubes[(x,y,0)] = True

search = list(itertools.product([0,1,-1],[0,1,-1],[0,1,-1]))
search.remove((0,0,0)) # Remove self

def iterate(structure):
	neigbourCount = {}
	for c in structure:
		for s in search:
			coord = tuple(map(sum, zip(s, c)))
			if coord not in neigbourCount:
				neigbourCount[coord] = 1
			else:
				neigbourCount[coord] += 1

	for n in neigbourCount:
		if n in structure: # Assume is true if it is in structure
			if not 2 <= neigbourCount[n] <= 3:
				structure[n] = False
		if neigbourCount[n] == 3:
			structure[n] = True
	for n in structure:
		if n not in neigbourCount:
			structure[n] = False

	for k, value in dict(structure).items(): #Prune data
		if not value:
			del structure[k]
	return structure

for i in range(6):
	cubes = iterate(cubes)

print(len(cubes))
