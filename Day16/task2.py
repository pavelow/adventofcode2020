#! /usr/bin/env python3
import parse
with open("input.txt") as f:
	data = f.read().split('\n\n')

for i in range(len(data)):
	data[i] = data[i].splitlines()

tickets = [list(map(int, t.split(','))) for t in data[2][1:]]

rules = {}		# Create rules sets
for rule in data[0]:
	a = rule.split(':')
	r = list(parse.parse("{:d}-{:d} or {:d}-{:d}",a[1]))
	rules[a[0]] = set()
	[rules[a[0]].add(n) for n in range(r[0],r[1]+1)]
	[rules[a[0]].add(n) for n in range(r[2],r[3]+1)]

allRules = set()    # Create valid set
validTickets = []
for rule in rules.values():
	allRules = allRules.union(rule)

validTickets = [ticket for ticket in tickets if all(n in allRules for n in ticket)] # Filter by tickets by valid set
fields = {i: {j[i] for j in validTickets} for i in range(len(validTickets[0]))} # Create set per field
fieldRules = {i: {j for j in rules if fields[i] <= rules[j]} for i in fields} # Find possible rules for each field


solved = {}   # Round down to actual rules for each field
done = set()
while len(solved) != len(fieldRules):
	for r in fieldRules:
		if(len(fieldRules[r]) == 1):
			rule = fieldRules[r].pop()
			solved[rule] = r
			done.add(rule)
	for r in fieldRules:
		fieldRules[r] = fieldRules[r] - done


my = [int(i) for i in data[1][1].split(',')] # Multiply numbers with "departure" in field name
ans = 1
for field in solved:
	if("departure" in field):
		ans *= my[solved[field]]
print(ans)
