#! /usr/bin/env python3
import parse
with open("input.txt") as f:
	data = f.read().split('\n\n')

for i in range(len(data)):
	data[i] = data[i].splitlines()


#rules = {}
rset = set()

for rule in data[0]:
	a = rule.split(':')
	r = list(parse.parse("{:d}-{:d} or {:d}-{:d}",a[1]))
	[rset.add(n) for n in range(r[0],r[1]+1)]
	[rset.add(n) for n in range(r[2],r[3]+1)]

errorRate = 0
for ticket in data[2][1:]:
	tNumbers = list(map(int, ticket.split(',')))
	for n in tNumbers:
		if n not in rset:
			errorRate += n
print(errorRate)
