#! /usr/bin/env python3

with open("input.txt") as f:
	data = f.read().splitlines()

def tokenize(line):
	tokens = []
	subtoken = ""
	brack = 0
	for char in line:
		if char not in " ()" and brack == 0:
			tokens.append(char)
		if char == "(":
			brack += 1
		if brack > 0:
			subtoken += char
		if char == ")":
			brack -= 1
			if brack == 0:
				tokens.append(subtoken[1:-1])
				subtoken = []
	return tokens


def evaluate(line):
	t = tokenize(line)
	b = 0
	for i in range(len(t)):
		if len(t[i]) > 1: # If we got a nested expression
			t[i] = evaluate(t[i])
	i = 0
	while i < len(t): # Additions first
		if '+' in t[i:(i+3)]:
			b = eval("".join(t[i:i+3]))
			t = t[:i] + [str(b)] + t[i+3:]
		else:
			i += 2
	while len(t) > 1: # Everything else, i.e. multiplication - can get away with this since there are only two operators
		b = eval("".join(t[:3]))
		t = [str(b)] + t[3:]
	return t[0]

s = 0
for l in data:
	s += int(evaluate(l))
print(s)
