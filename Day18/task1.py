#! /usr/bin/env python3

with open("input.txt") as f:
	data = f.read().splitlines()

def tokenize(line):
	tokens = []
	subtoken = ""
	brack = 0
	for char in line:
		if char not in " ()" and brack == 0:
			tokens.append(char)
		if char == "(":
			brack += 1
		if brack > 0:
			subtoken += char
		if char == ")":
			brack -= 1
			if brack == 0:
				tokens.append(subtoken[1:-1])
				subtoken = []
	return tokens


def evaluate(line):
	t = tokenize(line)
	b = 0
	for i in range(len(t)):
		if len(t[i]) > 1:
			t[i] = evaluate(t[i])
	while len(t) > 1:
		b = eval("".join(t[:3]))
		t = [str(b)] + t[3:]
	return t[0]

s = 0
for l in data:
	s += int(evaluate(l))
print(s)
