#! /usr/bin/env python3
import copy

with open("input.txt") as f:
	data = list(f.read().split('\n')[:-1])

for i in range(len(data)):
	data[i] = '.'+data[i] + '.'
data.insert(0,'.'*(len(data[0])))
data.append('.'*(len(data[0])))

directions = [[0,1],[1,0],[-1,0],[0,-1],[-1,-1],[1,-1],[1,1],[-1,1]]

data = [list(d) for d in data]

def Mutate(data):
	d = copy.deepcopy(data)
	for row, r in enumerate(data):
		for col, value in enumerate(r):
			s = 0
			for dir in directions:
				if value != '.':
					i = 1
					while True:
						if  0 <= row+(dir[0]*i) < len(data) and 0 <= col+(dir[1]*i) < len(data[0]):
							cmp = data[row+(dir[0]*i)][col+(dir[1]*i)]
							if cmp == '#':
								s+=1
								break
							elif cmp == 'L':
								break
						else:
							break
						i+=1
			if(value == 'L' and s == 0):
				d[row][col] = '#'
			elif(value == '#' and s >= 5):
				d[row][col] = 'L'
	return d

n = data
while True:
	m = Mutate(n)
	if(n != m):
		n = m
	else:
		count = 0
		for line in n:
			count += line.count('#')
		print(count)
		break
