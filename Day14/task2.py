#! /usr/bin/env python3
from parse import *

with open("input.txt") as f:
	data = f.read().splitlines()

mask = ""
mem = {}

for line in data:
	if "mask" in line:
		mask = line.split(" ")[2] # little endian
	if "mem" in line:
		location, value = (parse("mem[{:d}] = {:d}",line))
		binvalue = bin(location).split('b')[1].zfill(36)
		maskedvalue = ''
		for i, v in enumerate(mask):
			if v != '0':
				maskedvalue += v#binvalue[i]
			else:
				maskedvalue += binvalue[i]

		p = [pos for pos, char in enumerate(maskedvalue) if char == 'X']
		for i in range(2**maskedvalue.count('X')):
			m = list(maskedvalue)
			b = list(bin(i).split('b')[1].zfill(2**maskedvalue.count('X')))
			b.reverse()
			for j, pos in enumerate(p):
				m[pos] = b[j]
			mem[int("".join(m),2)] = value

print(sum(mem.values()))
