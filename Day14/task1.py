#! /usr/bin/env python3
from parse import *

with open("input.txt") as f:
	data = f.read().splitlines()

mask = ""
mem = {}

for line in data:
	if "mask" in line:
		mask = line.split(" ")[2] # little endian
	if "mem" in line:
		location, value = (parse("mem[{:d}] = {:d}",line))
		binvalue = bin(value).split('b')[1].zfill(36)
		maskedvalue = ''
		for i, v in enumerate(mask):
			if(v == 'X'):
				maskedvalue += binvalue[i]
			else:
				maskedvalue += v
		mem[location] = int(maskedvalue,2)


print(sum(mem.values()))
