#! /usr/bin/env python3
from parse import *
import math

data = []
with open("input.txt") as f:
	for line in f:
		data.append(list(parse("{}{:n}",line)))
facing = 0
x = 0
y = 0

for step in data:
	if(step[0] == 'F'):
		x += math.cos(math.radians(facing))*step[1]
		y += math.sin(math.radians(facing))*step[1]
	if(step[0] == 'L'):
		facing += step[1]
	if(step[0] == 'R'):
		facing -= step[1]
	if(step[0] == 'S'):
		y -= step[1]
	if(step[0] == 'N'):
		y += step[1]
	if(step[0] == 'E'):
		x += step[1]
	if(step[0] == 'W'):
		x -= step[1]

print(round(abs(x)+abs(y)))
