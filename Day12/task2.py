#! /usr/bin/env python3
from parse import *
import math

data = []
with open("input.txt") as f:
	for line in f:
		data.append(list(parse("{}{:n}",line)))

sX = 0
sY = 0

wpX = 10
wpY = 1

def rotateWP(a, wpX, wpY):
	length = math.sqrt(wpY**2 +wpX**2)
	angle = math.degrees(math.atan2(wpY,wpX)) # :')
	wpX = round(math.cos(math.radians(a+angle))*length)
	wpY = round(math.sin(math.radians(a+angle))*length)
	return wpX, wpY

for step in data:
	if(step[0] == 'F'):
		sX += step[1]*wpX
		sY += step[1]*wpY
	if(step[0] == 'L'):
		wpX, wpY = rotateWP(step[1], wpX, wpY)
	if(step[0] == 'R'):
		wpX, wpY = rotateWP(-step[1], wpX, wpY)
	if(step[0] == 'S'):
		wpY -= step[1]
	if(step[0] == 'N'):
		wpY += step[1]
	if(step[0] == 'E'):
		wpX += step[1]
	if(step[0] == 'W'):
		wpX -= step[1]

print((abs(sX)+abs(sY)))
