#! /usr/bin/env python3

with open("input.txt") as f:
	data = list(map(int, f.read().split('\n')[:-1]))
data.append(0)
data.sort()
data.reverse()

def Count(n, d):
	count = 0
	if(n == len(d)-1):
		return 1
	if(n <= len(d)-1):
		for i in range(1,4):
			if(n+i <= len(d)-1 and d[n]-d[n+i] <= 3):
				count += Count(n+i, d)
	return count

c = [0]
for i, value in enumerate(data):
	if i != 0 and data[i-1]-value == 3:
		c.append(i)
c.append(len(data))

acc = 1
for i in range(len(c)-1):
	acc *= Count(0, data[c[i]:c[1+i]])
print(acc)
