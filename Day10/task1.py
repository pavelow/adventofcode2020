#! /usr/bin/env python3

with open("input.txt") as f:
	data = list(map(int, f.read().split('\n')[:-1]))
data.sort()

builtin = max(data)+3

differences = {1:0, 2:0, 3:0}

for i, value in enumerate(data):
	if i == 0:
		differences[value] += 1
	else:
		differences[value - data[i-1]] += 1
differences[3] += 1

print(differences[1]*differences[3])
