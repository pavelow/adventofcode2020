#! /usr/bin/env python3
from parse import *

nPass = 0
with open("input.txt") as f:
	for passwd in f:
		a = parse("{}-{} {}: {}", passwd)
		nPass += ( (a[3][int(a[0])-1] == a[2]) ^ (a[3][int(a[1])-1] == a[2]) )

print(nPass)
