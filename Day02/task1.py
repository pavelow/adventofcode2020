#! /usr/bin/env python3
from parse import *

nPass = 0
with open("input.txt") as f:
	for passwd in f:
		a = parse("{}-{} {}: {}", passwd)
		count = a[3].count(a[2])
		nPass += ( count >= int(a[0]) and count <= int(a[1]))

print(nPass)	
