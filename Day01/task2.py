#! /usr/bin/env python3
from copy import deepcopy

n = []

with open("input.txt") as f:
	for line in f:
		n.append(int(line))

while len(n) > 0:
	t = n.pop()
	nn = deepcopy(n)
	while len(nn) > 0:
		p = nn.pop()
		for i in nn:
			if t+p+i == 2020:
				print(t*p*i)
				break
