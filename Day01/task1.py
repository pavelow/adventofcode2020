#! /usr/bin/env python3

n = []

with open("input.txt") as f:
	for line in f:
		n.append(int(line))

while len(n) > 0:
	t = n.pop()
	for j in n:
		if j+t == 2020:
			print(j*t)
			break
