#! /usr/bin/env python3

data = []
with open("input.txt") as f:
    for line in f:
        data.append(list(line.strip('\n')))

x = 0
y = 0
count = 0

while y < len(data):
    x += 3 
    y += 1 
    if( y < len(data)):
        if(data[y][x%len(data[0])] == '#'):
                count += 1
print(count)
