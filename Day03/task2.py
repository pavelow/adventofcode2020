#! /usr/bin/env python3

data = []
with open("input.txt") as f:
    for line in f:
        data.append(list(line.strip('\n')))

def checkSlope(pX, pY):
    x = 0
    y = 0
    count = 0

    while y < len(data):
        x += pX
        y += pY
        if( y < len(data)):
            if(data[y][x%len(data[0])] == '#'):
                count += 1
    return count

print(checkSlope(1,1)*checkSlope(3,1)*checkSlope(5,1)*checkSlope(7,1)*checkSlope(1,2))
