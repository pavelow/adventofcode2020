#! /usr/bin/env python3
import itertools as it
import copy

with open("input.txt") as f:
	foods = [[set(i.split("(")[0].strip().split(' ')) , i.split("(")[1].replace("contains", '')[:-1].strip().split(', ') ] for i in f.read().splitlines()]
allergins = set(it.chain(*[ food[1] for food in foods ]))
ingredients = set()
for ing in [food[0] for food in foods]:
	ingredients = ingredients.union(ing)

possibleContaining = set()
for allergin in allergins:
	possibleIngredients = ingredients
	for food in [food for food in foods if (allergin in food[1]) ]:
		possibleIngredients = possibleIngredients.intersection(food[0])
	possibleContaining = possibleContaining.union(possibleIngredients)

notContaining = (ingredients - possibleContaining)

sum = 0
for food in foods:
	for ing in notContaining:
		if ing in food[0]:
			sum += 1
print(sum)
