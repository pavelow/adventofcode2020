#! /usr/bin/env python3
import itertools as it
import copy

with open("input.txt") as f:
	foods = [[set(i.split("(")[0].strip().split(' ')) , i.split("(")[1].replace("contains", '')[:-1].strip().split(', ') ] for i in f.read().splitlines()]
allergins = set(it.chain(*(food[1] for food in foods)))
ingredients = set().union(*(food[0] for food in foods))

possibleContaining = set()
dangerous = {a: "" for a in allergins}
for allergin in allergins:
	possibleIngredients = ingredients
	for food in [food for food in foods if (allergin in food[1])]:
		possibleIngredients = possibleIngredients.intersection(food[0])
	dangerous[allergin] = possibleIngredients
	possibleContaining = possibleContaining.union(possibleIngredients)

notContaining = ingredients - possibleContaining

for allergin in dangerous:
	dangerous[allergin] = dangerous[allergin] - notContaining

while not all(len(dangerous[allergy])==1 for allergy in dangerous):
	for a1 in (d for d in dangerous if len(dangerous[d]) == 1):
		for a2 in (d for d in dangerous if len(dangerous[d]) > 1):
				dangerous[a2] = dangerous[a2] - dangerous[a1]

print(",".join(["".join(dangerous[allergin]) for allergin in sorted(dangerous)]))
