#! /usr/bin/env python3
import copy

instructions = []
with open("input.txt") as f:
	instructions = f.read().split('\n')

def execute(inst):
	acc = 0
	pc = 0
	executed = {}
	while True:
		if(inst[pc] == ''):
			print(acc)
			return True
		opcode, data = inst[pc].split(" ")
		if pc in executed:
			return False
		else:
			executed[pc]=1
		lastpc = pc
		if(opcode == "nop"):
			pc+=1
		if(opcode == "acc"):
			acc += int(data)
			pc+=1
		if(opcode == "jmp"):
			pc += int(data)

for i in range(len(instructions)):
	if ("jmp" in instructions[i]):
		j = copy.copy(instructions)
		j[i] = j[i].replace("jmp", "nop")
		if(execute(j)):
			break
	elif "nop" in instructions[i]:
		j = copy.copy(instructions)
		j[i] = j[i].replace("nop", "jmp")
		if(execute(j)):
			break
