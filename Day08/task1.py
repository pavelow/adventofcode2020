#! /usr/bin/env python3

instructions = []
with open("input.txt") as f:
	instructions = f.read().split('\n')

acc = 0
pc = 0
executed = {}


while True:
	opcode, data = instructions[pc].split(" ")
	if pc in executed:
		print(acc)
		break
	else:
		executed[pc]=1
	if(opcode == "nop"):
		pc+=1
	if(opcode == "acc"):
		acc += int(data)
		pc+=1
	if(opcode == "jmp"):
		pc += int(data)
