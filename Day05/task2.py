#! /usr/bin/env python3

IDs = []

with open("input.txt") as f:
	for line in f:
		IDs.append(eval("0b"+line.replace("F","0").replace("B","1").replace("R","1").replace("L","0")))
	IDs.sort();

for i in range(len(IDs)):
	if(IDs[i+1] != IDs[i]+1):
		print(IDs[i]+1)
		break
