#! /usr/bin/env python3

max = 0

with open("input.txt") as f:
	for line in f:
		ID = eval("0b"+line.replace("F","0").replace("B","1").replace("R","1").replace("L","0"))
		if (ID > max):
			max = ID
print(max)
