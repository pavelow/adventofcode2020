# Advent of Code 2020
Advent of code is a yearly advent calendar with new programming problems each day.
This repository contains my solutions to the [AoC 2020 problems](https://adventofcode.com/2020).

My 2019 solutions can be found [here](https://gitlab.com/pavelow/adventofcode2019).
