#! /usr/bin/env python3
with open("input.txt") as f:
	data = [int(i) for i in f.read().replace('\n','').split(',')]


for i in range(len(data),2020):
	c = data.count(data[-1])
	if c == 1:
		data.append(0)
	elif c > 1:
		lastIndexes = [i+1 for i, value in enumerate(data) if value == data[-1]][-2:]
		data.append(lastIndexes[1]-lastIndexes[0])

print(data[-1])
