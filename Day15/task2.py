#! /usr/bin/env python3
with open("input.txt") as f:
	data = [int(i) for i in f.read().replace('\n','').split(',')]

l = {}
ll = {}
count = {}
for i, value in enumerate(data):
	l[value] = i+1
	ll[value] = i+1
	if(value not in count):
		count[value] = 1
	else:
		count[value] += 1

for i in range(len(data), 30000000):
	if( i%1000000 == 0):
		print("{:.2f}%...".format((i/30000000)*100))
	c = count[data[-1]]
	if c == 1:
		ll[0] = l[0]
		l[0] = len(data)+1
		count[0] += 1
		data.append(0)
	elif c > 1:
		result = l[data[-1]] - ll[data[-1]]
		if(result in l):
			ll[result] = l[result]
			l[result] = len(data)+1
		else:
			l[result] = len(data)+1
			ll[result] = l[result]
		if result in count:
			count[result] += 1
		else:
			count[result] = 1
		data.append(result)

print(data[-1])
