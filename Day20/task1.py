#! /usr/bin/env python3
import math

with open("input.txt") as f:
	tiles = {int(tile.split(":")[0].split(" ")[1]): tile.split(":")[1].splitlines()[1:] for tile in f.read().split('\n\n')}

completedTiles = set()
tileMatches = {}

def getEdges(tileID): #return list of edges clockwise from top T,R,B,L
	edges = {}
	edges['T'] = "".join([tiles[tileID][0]])
	edges['R'] = "".join([t[-1] for t in tiles[tileID]])
	edges['B'] = tiles[tileID][-1]
	edges['L'] = "".join([t[0] for t in tiles[tileID]])
	return edges

def checkEdges(localEdges, tileIDforeign): # return side to fit on and required rotation
	foreignEdges = getEdges(tileIDforeign)
	matches = []
	for side in localEdges:
		for fside in foreignEdges:
			if localEdges[side] == foreignEdges[fside]:
				matches.append((side))
			if localEdges[side] == "".join(reversed(foreignEdges[fside])):
				matches.append((side))
	return matches

def fit(tileID):
	thisTileEdges = getEdges(tileID)
	for otherTileID in [t for t in tiles if t != tileID]: #not in completedTiles
		edgeMatches = checkEdges(thisTileEdges, otherTileID)
		if edgeMatches:
			if tileID not in tileMatches:
				tileMatches[tileID] = {}
			tileMatches[tileID][otherTileID] = checkEdges(thisTileEdges, otherTileID)

for tileID in tiles: # find edge matches
	fit(tileID)

print(math.prod(tileID for tileID in tileMatches if len(tileMatches[tileID]) == 2))
