#! /usr/bin/env python3

with open("input.txt") as f:
	tiles = {int(tile.split(":")[0].split(" ")[1]): tile.split(":")[1].splitlines()[1:] for tile in f.read().split('\n\n')}

tileMatches = {}
sideLength = int(len(tiles)**0.5)
completedTiles = set()

def getEdges(tileID): #return list of edges clockwise from top T,R,B,L
	edges = {}
	edges['T'] = "".join([tiles[tileID][0]])
	edges['R'] = "".join([t[-1] for t in tiles[tileID]])
	edges['B'] = "".join(tiles[tileID][-1])
	edges['L'] = "".join([t[0] for t in tiles[tileID]])
	return edges

def rotateTile90(tileID, n): # rotate tile anticlockwise 90 degrees some number of times
	for i in range(n):
		tiles[tileID] = [ "".join([t[i] for t in reversed(tiles[tileID])]) for i in range(len(tiles[tileID][0]))]

def flipV(tileID):
	tiles[tileID].reverse()

def flipH(tileID):
	for i in range(len(tiles[tileID])):
		tiles[tileID][i] = tiles[tileID][i][::-1]

def checkEdges(localEdges, tileIDforeign): # return side to fit on and required rotation
	foreignEdges = getEdges(tileIDforeign)
	matches = []
	for side in localEdges:
		for fside in foreignEdges:
			if localEdges[side] == foreignEdges[fside]:
				matches = ((side, 'f'))
			if localEdges[side] == foreignEdges[fside][::-1]:
				matches = ((side, 'r'))
	return matches

def fit(tileID):
	matches = {}
	thisTileEdges = getEdges(tileID)
	for otherTileID in [t for t in tiles if (t != tileID and t not in completedTiles)]: #not in completedTiles
		edgeMatches = checkEdges(thisTileEdges, otherTileID)
		if edgeMatches:
			matches[otherTileID] = edgeMatches
	return matches

def getACorner(): # Get a corner
	for tileID in tiles: # find edge matches
		if len(fit(tileID)) == 2:
			makeTopLeft(tileID)
			return tileID

def makeTopLeft(tileID): # Make corner be top left by flipping
	f = list(fit(tileID).values())
	if( 'T' in (i[0][0] for i in f)):
		flipV(tileID)
	if( 'L' in (i[0][0] for i in f)):
		flipH(tileID)

def tileFits(x,y, Map): #Returns boolean
	matches = {}
	thisTileEdges = getEdges(Map[x][y])
	check = [Map[x-1][y], Map[x][y-1]]
	for otherTileID in [tile for tile in check if tile != -1]:
		edgeMatches = checkEdges(thisTileEdges, otherTileID)
		if edgeMatches:
			matches[otherTileID] = edgeMatches
	if Map[x-1][y] in matches and matches[Map[x-1][y]] != ('L', 'f'):
		return False
	if Map[x][y-1] in matches and matches[Map[x][y-1]] != ('T', 'f'):
		return False
	return True

def checkRotations(x, y, Map):
	for i in range(4):
		rotateTile90(Map[x][y], 1)
		if(tileFits(x,y, Map)):
			return True
	return False

def makeTileFit(x,y, Map): # Brute force it
	if(checkRotations(x,y,Map)):
		return True
	flipH(Map[x][y])
	if(checkRotations(x,y,Map)):
		return True
	flipV(Map[x][y])
	if(checkRotations(x,y,Map)):
		return True
	flipH(Map[x][y])
	if(checkRotations(x,y,Map)):
		return True

def findMatch(x, y, Map, dim): # find match and populate
	selection = {}

	if Map[x-1][y] != -1:
		selection['L'] = fit(Map[x-1][y])
	if Map[x][y-1] != -1:
		selection['U'] = fit(Map[x][y-1])
	if len(selection) == 1: # Only one adjacency
		side, value = selection.popitem()
		value = {v[0]: (k, v[1]) for k,v in value.items()}
		tileID = value['R' if side == 'L' else 'B'][0]
		completedTiles.add(tileID)
		Map[x][y] = tileID
		return
	else: # Selection has to be two
		tileID = set(selection['L'].keys()).intersection(set(selection['U'].keys())).pop()
		completedTiles.add(tileID)
		Map[x][y] = tileID
		return

def findMap(TopLeftID): # Build adjacency map in a diagonal sweep
	sideLength = int(len(tiles)**0.5)
	Map = [[-1 for i in range(sideLength)] for i in range(sideLength)]
	Map[0][0] = TopLeftID
	for i in range(1,(sideLength*2)-1):
		y = 0
		for x in range(i,-1,-1):
			if x < sideLength and y < sideLength:
				findMatch(x, y, Map, sideLength)
				makeTileFit(x,y, Map)
			y += 1
	return Map

Corner = getACorner() # We have our top left anchor now
completedTiles.add(Corner)
tileMap = findMap(Corner) # We have our adjacency map now

# for line in  [ [t[i] for t in tileMap] for i in range(sideLength)]: # Transpose because of swapping x and y
# 	print(line)

# Build Image
image = []
for y in range(sideLength):
	for tileY in range(1,9):
		line = ""
		for x in range(sideLength):
			line += tiles[tileMap[x][y]][tileY][1:-1]
		image.append(line)

# Do pattern matching
pattern = [	"                  # ",
			"#    ##    ##    ###",
			" #  #  #  #  #  #   "]

def IRotate90(Map): # rotate tile anticlockwise 90 degrees some number of times
	return [ "".join([t[i] for t in reversed(Map)]) for i in range(len(Map[0]))]

def IflipV(Map):
	Map.reverse()
	return Map

def IflipH(Map):
	for i in range(len(Map)):
		Map[i] = Map[i][::-1]
	return Map

def patternSearch(image, pattern):
	visited = set()
	matches = 0
	for x in range(len(image[0])):
		for y in range(len(image)):
			if y + len(pattern) <= len(image) and x + len(pattern[0]) <= len(image[0]):
				match = True
				subVisited = set()
				for i, patternLine in enumerate(pattern):
					imageLine = image[y+i][x:x+len(pattern[0])]
					for j, char in enumerate(patternLine):
						if char == "#" and char != imageLine[j]:
							match = False
							break
						elif char == "#":
							subVisited.add((y+i, x+j))
				if match:
					visited = visited.union(subVisited)
					matches += match
	return len(visited)

def rotationSearch(image):
	for i in range(4):
		if i := patternSearch(image, pattern):
			return i
		image = IRotate90(image)

def orientationSearch(image):
	if i := rotationSearch(image): return i
	image = IflipH(image)
	if i := rotationSearch(image): return i
	image = IflipV(image)
	if i := rotationSearch(image): return i
	image = IflipH(image)
	if i := rotationSearch(image): return i

nCharMonster = orientationSearch(image) #We have the correct orientation now
TotalHashes = sum(line.count("#") for line in image)
print(TotalHashes-nCharMonster)
