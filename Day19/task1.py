#! /usr/bin/env python3
import re

with open("input.txt") as f:
	data = f.read().split('\n\n')

r = {int(line.split(':')[0]): line.split(':')[1].strip() for line in data[0].split('\n')} # Parse rules

def parseRule(rule): # Return regex for rule recursively
	if '"' in rule:
		return rule.replace('"','')
	elif '|' in rule: #Or
		return '(' + '|'.join(parseRule(r.strip()) for r in rule.split("|")) + ')'
	else: #Numbers
		return "".join([parseRule(r[int(i)]) for i in rule.split(" ")])

def check(msg, regex):
	return regex.fullmatch(msg) is not None

regex = re.compile(parseRule(r[0]))
print(sum(check(message, regex) for message in data[1].splitlines()))
