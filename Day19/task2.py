#! /usr/bin/env python3
import re

with open("input.txt") as f:
	data = f.read().split('\n\n')

r = {int(line.split(':')[0]): line.split(':')[1].strip() for line in data[0].split('\n')} # Parse rules

def parseRule(rule): # Return regex for rule recursively
	if '"' in rule:
		return rule.replace('"','')
	elif '|' in rule: #Or
		return '(' + '|'.join(parseRule(r.strip()) for r in rule.split("|")) + ')'
	else: #Numbers
		return "".join([parseRule(r[int(i)]) for i in rule.split(" ")])

def consume(msg, r):
	end = count = 0
	while (rm := r.match(msg[end:])):
		count += 1
		end += rm.span()[1]
	return count, msg[end:]

def check(msg): # Expect two or more rule 42s followed by one or more rule 31s
	m42c, msg = consume(msg, re.compile(parseRule(r[42]))) # Count and consume all leading rule 42s
	m31c, msg = consume(msg, re.compile(parseRule(r[31]))) # Count and consume all leading rule 31s
	return m42c > m31c > 0 and not msg

print(sum(check(msg) for msg in data[1].splitlines()))
