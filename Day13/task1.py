#! /usr/bin/env python3
import math

with open("input.txt") as f:
	timestamp = int(f.readline())
	busses = [int(b) for b in f.readline().replace('\n','').split(',') if b !='x']

a = [(math.ceil(timestamp/id) * id) for id in busses]
print((min(a)-timestamp) * busses[a.index(min(a))])
