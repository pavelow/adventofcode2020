#! /usr/bin/env python3
import math

with open("input.txt") as f:
	busses = []
	for i, value in enumerate(b for b in f.read().split('\n')[1].replace('\n','').split(',')):
		if value != 'x': busses.append([i, int(value)])

def BaseInterval(b):
	if(len(b) == 1):
		return b[0][0]
	t = b[0][0]
	while (t+b[1][0])%b[1][1] != 0:
		t += b[0][1]
	base = t
	t += b[0][1]
	while (t+b[1][0])%b[1][1] != 0:
		t += b[0][1]
	a = [[base,t-base]]
	a.extend(b[2:])
	return BaseInterval(a)

print(BaseInterval(busses))
