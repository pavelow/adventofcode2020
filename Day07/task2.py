#! /usr/bin/env python3
data = []

with open("input.txt") as f:
	data = f.read().split('\n')
	for l in data:
		l = l.strip();


rules = {}
data.remove('')
for rule in data:
	a = rule.split("contain")
	b = "".join(a[0].split(" ")[:-2])
	rules[b] = []
	for r in a[1].split(","):
		rules[b].append("".join(r.split(" ")[1:-1]))

def NestedBags(colour):
	check = rules[colour]
	count = 0

	for col in check:
		if(col != 'noother'):
			n = int(col[:1])
			count += n*(1+NestedBags(col[1:]))
	return count

print(NestedBags("shinygold"))
