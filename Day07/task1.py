#! /usr/bin/env python3
data = []

with open("input.txt") as f:
	data = f.read().split('\n')
	for l in data:
		l = l.strip();


rules = {}
data.remove('')
for rule in data:
	a = rule.split("contain")
	b = "".join(a[0].split(" ")[:-2])
	rules[b] = []
	for r in a[1].split(","):
		rules[b].append("".join(r.split(" ")[1:-1][-2:]))

def contains(chk):
	keys = set()
	for key in rules:
		for rule in rules[key]:
			if(rule == chk):
				keys.add(key)
	if(len(keys) > 0):
		for k in keys:
			keys = keys.union(contains(k))
	return keys

print(len(contains("shinygold")))
