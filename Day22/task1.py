#! /usr/bin/env python3
import collections

with open("input.txt") as f:
	cards = {int(player.split(":")[0].split()[1]): collections.deque(map(int,player.split(":")[1].splitlines()[1:]))  for player in f.read().split('\n\n')}

def Round(cards):
	p = {1:cards[1].popleft(), 2:cards[2].popleft()}
	if p[1] > p[2]:
		cards[1].append(p[1])
		cards[1].append(p[2])
		return cards, 1
	else:
		cards[2].append(p[2])
		cards[2].append(p[1])
		return cards, 2

winner = 0
while all(len(card) > 0 for card in cards.values()):
	cards, winner = Round(cards)

print(sum((i+1)*value for i, value in enumerate(reversed(cards[winner]))))
