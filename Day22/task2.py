#! /usr/bin/env python3
import collections, copy
import itertools as it

with open("input.txt") as f:
	cards = {int(player.split(":")[0].split()[1]): collections.deque(map(int,player.split(":")[1].splitlines()[1:]))  for player in f.read().split('\n\n')}

def Round(cards):
	p = {1:cards[1].popleft(), 2:cards[2].popleft()}
	if(len(cards[1]) >= p[1] and len(cards[2]) >= p[2]):
		c = copy.deepcopy(cards)
		c[1] = collections.deque(list(c[1])[:p[1]]) #woo
		c[2] = collections.deque(list(c[2])[:p[2]])
		n, winner = playGame(c)
		cards[winner].append(p[winner])
		cards[winner].append(p[1 if winner == 2 else 2])
		return cards, winner
	else:
		if p[1] > p[2]:
			cards[1].append(p[1])
			cards[1].append(p[2])
			return cards, 1
		else:
			cards[2].append(p[2])
			cards[2].append(p[1])
			return cards, 2


def playGame(cards):
	usedDecks = set()
	while all(cards.values()):
		if (tuple(cards[1]), tuple(cards[2])) in usedDecks: # Check for loop
			return cards, 1
		else:
			usedDecks.add((tuple(cards[1]), tuple(cards[2])))
			cards, winner = Round(cards)
	return cards, winner

cards, win = playGame(cards)
print(sum((i+1)*value for i, value in enumerate(reversed(cards[win]))))
