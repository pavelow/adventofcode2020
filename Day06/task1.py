#! /usr/bin/env python3

data = []
with open("input.txt") as f:
	groups = f.read().split("\n\n")
	for group in groups:
		g = group.split("\n")
		if '' in g : g.remove('')
		data.append(g)

count = 0
for group in data:
	yes = set()
	for person in group:
		for answer in person:
			yes.add(answer)
	count += len(yes)

print(count)
