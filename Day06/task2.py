#! /usr/bin/env python3
import collections


data = []
with open("input.txt") as f:
	groups = f.read().split("\n\n")
	for group in groups:
		g = group.split("\n")
		if '' in g : g.remove('')
		data.append(g)

count = 0
for group in data:
	g = collections.Counter(''.join(group))
	for i in g:
		if g[i] == len(group):
			count += 1

print(count)
