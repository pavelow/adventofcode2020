#! /usr/bin/env python3
import collections

with open("input.txt") as f:
	cups = collections.deque(map(int, list(f.readline())[:-1]))

def Turn():
	cups.rotate(-1)
	selection = [cups.popleft() for i in range(3)]
	cups.rotate(1)
	next = cups[1]
	destination = cups[0]-1
	while destination not in cups:
		destination -= 1
		if destination < min(cups):
			destination = max(cups)
	cups.rotate(-cups.index(destination)-1)
	cups.extendleft(reversed(selection))
	cups.rotate(-cups.index(next))

for i in range(100):
	Turn()
cups.rotate(-cups.index(1))
print("".join(list(map(str,cups))[1:]) )
