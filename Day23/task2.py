#! /usr/bin/env pypy3

with open("input.txt") as f:
	cupsInput =  list(map(int, list(f.readline())[:-1]))

class llElement:
	def __init__(self, prev, value, next):
		self.next = next
		self.value = value
		self.prev = prev

class mappedLinkedList:
	def __init__(self, startingValue):
		self.first = llElement(0,startingValue,0)
		self.last = self.first = self.first.next = self.first.prev = self.first
		self.elementMap = {startingValue:self.first}
		self.min = self.max = startingValue

	def append(self,i):
		element = llElement(self.last, i, self.first)
		self.last.next = element
		self.last = element
		self.elementMap[i] = element
		if self.min > i:
			self.min = i
		if self.max < i:
			self.max = i

	def popNAfter(self,i, n):
		popped = [self.elementMap[i].next]
		self.elementMap.pop(popped[0].value)
		for j in range(n-1):
			popped.append(popped[-1].next)
			if i == popped[-1].value:
				breakpoint()
			self.elementMap.pop(popped[-1].value)
		self.elementMap[i].next = popped[-1].next #update pointer
		return [i.value for i in popped]

	def insertNAfter(self, i, l):
		first = element = llElement(self.elementMap[i], l[0], 0)
		self.elementMap[element.value] = element
		for j in l[1:]:
			element.next = llElement(element, j, 0)
			self.elementMap[j] = element.next
			element = element.next
		if self.elementMap[i].next == self.first:
			self.last = element
		element.next = self.elementMap[i].next
		self.elementMap[i].next = first

	def rotate(self,n): #rotate right
		if(n < 0): #walk forwards first N
			left = self.first
			right = left
			for i in range(abs(n)-1):
				right = right.next
			self.first = right.next
			self.last.next = left
			self.last = right
		else: # walk back N from last
			right = self.last
			left = right
			for i in range(n-1):
				left = left.prev
			self.first = left
			self.last = left

	def list(self):
		element = self.first
		l = []
		while True:
			l.append(element.value)
			element = element.next
			if element == self.first:
				return l

	def mini(self, selection):
		m = min(selection)
		if m == self.min:
			for j in range(3):
				if m+j in self.elementMap:
					return self.min+j
		else:
			return self.min

	def maxi(self, selection):
		if max(selection) == self.max:
			for j in range(3):
				if all(i for i in selection if i == self.max-j):
					return self.max-j
		else:
			return self.max

#populate cups
cups = mappedLinkedList(cupsInput[0])
for i in cupsInput[1:]:
	cups.append(i)

for i in range(max(cupsInput)+1, 1000001): # Add elements to the end
	cups.append(i)

def Turn():
	selection = cups.popNAfter(cups.first.value, 3)
	next = cups.first.value
	destination = next-1
	while destination not in cups.elementMap:
		destination -= 1
		if destination < cups.mini(selection):
			destination = cups.maxi(selection)
	cups.insertNAfter(destination, selection)
	cups.rotate(-1)

for i in range(10000000):
	if(i % 1000000 == 0):
		print("{}M".format(i/1000000))
	Turn()
n = cups.popNAfter(1,2)
print(n[0]*n[1])
