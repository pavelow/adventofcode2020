#! /usr/bin/env python3

data = []

with open("input.txt") as f:
    for line in f:
        data.append(line)

reqFields = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
l = []
count = 0
for i in range(len(data)):
    l += (data[i].strip("\n").split(" ")
    if data[i] == '\n' or i+1 == len(data):
        fields = set(map(lambda x : x.split(":")[0], l))
        if (fields.intersection(reqFields) == reqFields):
            count += 1
        l = []

print(count)
