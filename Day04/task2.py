#! /usr/bin/env python3
import re

data = []

with open("input.txt") as f:
    for line in f:
        data.append(line)

def validate(d):
    v = {}
    v["byr"] = int(d["byr"]) >= 1920 and int(d["byr"]) <= 2020
    v["iyr"] = int(d["iyr"]) >= 2010 and int(d["iyr"]) <= 2020
    v["eyr"] = int(d["eyr"]) >= 2020 and int(d["eyr"]) <= 2030
   
    hgt = re.search("^[0-9]+(cm|in)$", d["hgt"])
    if(hgt != None):
        unit = hgt.string[-2:]
        number = int(hgt.string[:-2])
        if( unit == "cm"):
            v["hgt"] = number >= 150 and number <= 193
        if( unit == "in"):
            v["hgt"] = number >= 59 and number <= 76
    else:
        v["hgt"] = False

    v["hcl"] = re.search("^\#[a-f,0-9]{6}$", d["hcl"]) != None
    v["ecl"] = re.search("^(amb|blu|brn|gry|grn|hzl|oth)$", d["ecl"]) != None
    v["pid"] = re.search("^[0-9]{9}$", d["pid"]) != None
   
    ret = True
    for i in list(v.values()):
        if i == False:
            ret = False
    return ret

reqFields = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
l = []
count = 0
for i in range(len(data)):
    if data[i] != '\n':
        l += (data[i].strip("\n").split(" "))
    if data[i] == '\n' or i+1 == len(data):
        fields = set(map(lambda x : x.split(":")[0], l))
        if (fields.intersection(reqFields) == reqFields):
            d = {}
            for f in l:
                kp = f.split(":")
                d[kp[0]] = kp[1]
            if(validate(d)):
                count += 1
        l = []

print(count)
