#! /usr/bin/env python3
import re, collections
from operator import add

with open("input.txt") as f:
	data = f.read().split('\n')[:-1]

tileRegex = re.compile("(e|w|se|sw|nw|ne)")
tiles = [tileRegex.split(i)[1::2] for i in data]

def resolveCoord(moves):
	coord = [0,0]
	m = {'e': (1,0), 'w':(-1,0), 'se':(0,-1), 'sw':(-1,-1), 'nw':(0,1), 'ne':(1,1)}
	for move in moves:
		coord = map(add, coord, m[move])
	return tuple(coord)

tileMap = collections.defaultdict(lambda: False)
for c in tiles:
	coord = resolveCoord(c)
	tileMap[coord] = not tileMap[coord]

print(sum([tileMap[i] for i in tileMap]))
