#! /usr/bin/env python3
import re, collections
from operator import add

with open("input.txt") as f:
	data = f.read().split('\n')[:-1]

tileRegex = re.compile("(e|w|se|sw|nw|ne)")
tiles = [tileRegex.split(i)[1::2] for i in data]
tileMap = collections.defaultdict(lambda: False)

def resolveCoord(moves):
	coord = [0,0]
	m = {'e': (1,0), 'w':(-1,0), 'se':(0,-1), 'sw':(-1,-1), 'nw':(0,1), 'ne':(1,1)}
	for move in moves:
		coord = map(add, coord, m[move])
	return tuple(coord)

def countAdjacent(tile, tileMap, flipMap):
	m = [(1,0), (-1,0), (0,-1), (-1,-1), (0,1), (1,1)]
	if tileMap[tile]:
		for c in [tuple(map(add, i, tile)) for i in m]:
			flipMap[c] += 1

def iterate(tileMap):
	flipMap = collections.defaultdict(lambda: 0)
	for i in tileMap:
		countAdjacent(i, tileMap, flipMap)
	for i in set(flipMap).union(tileMap):
		if tileMap[i] and (flipMap[i] == 0 or flipMap[i] > 2): #black tile with 0 or >2 adjacent flip to white
			tileMap[i] = False
		elif not tileMap[i] and flipMap[i] == 2: #white tile with 2 adjacent, flip to black
			tileMap[i] = True
	return tileMap

for c in tiles:
	coord = resolveCoord(c)
	tileMap[coord] = not tileMap[coord]

for i in range(100):
	tileMap = iterate(tileMap)
print(sum([tileMap[i] for i in tileMap]))
