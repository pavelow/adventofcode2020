#! /usr/bin/env pypy3
with open("input.txt") as f:
	doorP,cardP = map(int,f.read().splitlines())

def findLoops(pubkey,subject):
	c = 0
	value = 1*subject
	while True:
		value = loop(value,subject)
		c+=1
		if value == pubkey:
			return c

def loop(value, subject):
	value = subject*value
	return value%20201227

loopSize = findLoops(cardP,7)
value = 1
for i in range(loopSize+1):
	value = loop(value,doorP)
print(value)
