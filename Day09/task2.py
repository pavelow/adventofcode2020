#! /usr/bin/env python3
import copy

d = []
with open("input.txt") as f:
	d = list(map(int, f.read().split('\n')[:-1]))


def findInvalid(data):
	preamble = 25
	for i in range(preamble, len(data)):
		d = data[i-preamble:i]
		p = False
		while len(d) > 0:
			t = d.pop()
			for j in d:
				if j+t == data[i]:
					p = True
		if p == False:
			return data[i]

def findContiguousSum(data, n):
	start = 0
	while True:
		sum = 0
		i = 0
		while start+i < len(data):
			sum += data[start+i]
			if(sum == n):
				seq = data[start:start+i]
				return min(seq) + max(seq)
			i+=1
		start +=1


print(findContiguousSum(d, findInvalid(d)))
