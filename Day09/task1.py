#! /usr/bin/env python3
import copy

data = []
with open("input.txt") as f:
	data = list(map(int, f.read().split('\n')[:-1]))

preamble = 25

for i in range(preamble, len(data)):
	d = data[i-preamble:i]

	p = False
	while len(d) > 0:
		t = d.pop()
		for j in d:
			if j+t == data[i]:
				p = True;
	if p == False:
		print(data[i])
		break
